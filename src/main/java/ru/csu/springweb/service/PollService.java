package ru.csu.springweb.service;

import ru.csu.springweb.dto.Poll;

import java.time.LocalDate;
import java.util.List;

public interface PollService {
    void createPoll(Poll poll);

    void deletePoll(long id);

    ru.csu.springweb.model.Poll updatePoll(long id, Poll poll);

    ru.csu.springweb.model.Poll getPoll(long id);

    void addQuestionToPoll(long pollId, Object question);

    Object updateQuestion(long questionId, Object question);

    void deleteQuestion(long id);

    List<ru.csu.springweb.model.Poll> findPollsByDate(LocalDate start, LocalDate end);

}
