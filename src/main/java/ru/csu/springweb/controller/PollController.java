package ru.csu.springweb.controller;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import ru.csu.springweb.dto.Poll;
import ru.csu.springweb.service.PollService;

import javax.validation.Valid;

@RestController
@RequiredArgsConstructor
@RequestMapping("/polls")
public class PollController {

    private final PollService pollService;

    @PostMapping
    @ResponseStatus(code = HttpStatus.CREATED)
    public void createPoll(@RequestBody @Valid Poll poll) {
        pollService.createPoll(poll);
    }

    @GetMapping("/{id}")
    @Operation(description = "Получить опрос по его id")
    public ru.csu.springweb.model.Poll getPollById(@Parameter(description = "id опроса") @PathVariable long id) {
        return pollService.getPoll(id);
    }

    @DeleteMapping("/{id}")
    public void deletePoll(@PathVariable long id) {
        pollService.deletePoll(id);
    }

    @PutMapping("/{id}")
    public void updatePoll(@RequestBody Poll poll, long id) {
        pollService.updatePoll(id, poll);
    }
}
