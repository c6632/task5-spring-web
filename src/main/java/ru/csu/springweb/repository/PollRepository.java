package ru.csu.springweb.repository;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import ru.csu.springweb.entity.Poll;

import java.time.LocalDate;

public interface PollRepository extends CrudRepository<Poll, Long> {

    Poll findByPollName(String pollName);

    @Query("SELECT p FROM Poll p WHERE p.pollName = :name")
    Poll findPoll(@Param("name") String name);

    @Query(value = "SELECT * FROM poll p WHERE p.poll_name = :name", nativeQuery = true)
    Poll findPollNativeSql(@Param("name") String name);

    Poll findByStartDateBetween(LocalDate startDate, LocalDate endDate);
}
