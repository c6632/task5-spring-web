package ru.csu.springweb;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.junit.jupiter.Container;
import ru.csu.springweb.dto.Poll;
import ru.csu.springweb.repository.PollRepository;
import ru.csu.springweb.service.PollService;

import javax.persistence.EntityNotFoundException;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

@SpringBootTest
class SpringWebApplicationTests {

    @Autowired
    private PollService pollService;
    @Autowired
    private PollRepository pollRepository;

    @Test
    void createPollTest() {
        Poll poll = new Poll(
                "TestPoll",
                LocalDate.now(),
                LocalDate.now().plus(1, ChronoUnit.DAYS),
                true
        );
        pollService.createPoll(poll);
        var pollFromDb = pollRepository.findPoll("TestPoll");

        Assertions.assertEquals(poll.getPollName(), pollFromDb.getPollName());
        Assertions.assertEquals(poll.getStartDate(), pollFromDb.getStartDate());
        Assertions.assertEquals(poll.getEndDate(), pollFromDb.getEndDate());
        Assertions.assertEquals(poll.getActive(), pollFromDb.getActive());
    }

    @Test
    void getPollTest() {
        Assertions.assertThrows(EntityNotFoundException.class, () -> pollService.getPoll(1L));
        var testPoll = new ru.csu.springweb.entity.Poll(1L,
                "TestPoll",
                LocalDate.now(),
                LocalDate.now().plus(1, ChronoUnit.DAYS),
                true,
                List.of());
        pollRepository.save(testPoll);

        var poll = pollService.getPoll(1L);
        Assertions.assertEquals(testPoll.getId(), poll.getId());
        Assertions.assertEquals(testPoll.getStartDate(), poll.getStartDate());
        Assertions.assertEquals(testPoll.getEndDate(), poll.getEndDate());
        Assertions.assertEquals(testPoll.getPollName(), poll.getPollName());
    }

    @Container
    public static MySQLContainer<?> container = new MySQLContainer<>("mysql:latest")
            .withDatabaseName("integration-tests-db")
            .withUsername("sa")
            .withPassword("sa");

    @BeforeAll
    public static void start() {
        container.start();
    }

    @DynamicPropertySource
    public static void overrideProperties(DynamicPropertyRegistry dynamicPropertyRegistry) {
        dynamicPropertyRegistry.add("spring.datasource.url", container::getJdbcUrl);
        dynamicPropertyRegistry.add("spring.datasource.username", container::getUsername);
        dynamicPropertyRegistry.add("spring.datasource.password", container::getPassword);
    }
}
