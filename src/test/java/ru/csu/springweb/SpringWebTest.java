package ru.csu.springweb;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;
import org.testcontainers.containers.MySQLContainer;
import org.testcontainers.junit.jupiter.Container;
import ru.csu.springweb.repository.PollRepository;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;

@SpringBootTest
@AutoConfigureMockMvc
class SpringWebTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PollRepository pollRepository;

    @Container
    public static MySQLContainer<?> container = new MySQLContainer<>("mysql:latest")
            .withDatabaseName("integration-tests-db")
            .withUsername("sa")
            .withPassword("sa");

    @BeforeAll
    public static void start() {
        container.start();
    }

    @DynamicPropertySource
    public static void overrideProperties(DynamicPropertyRegistry dynamicPropertyRegistry) {
        dynamicPropertyRegistry.add("spring.datasource.url", container::getJdbcUrl);
        dynamicPropertyRegistry.add("spring.datasource.username", container::getUsername);
        dynamicPropertyRegistry.add("spring.datasource.password", container::getPassword);
    }

    @Test
    void getPollById() throws Exception {
        mockMvc
                .perform(MockMvcRequestBuilders.get("/polls/1"))
                .andExpect(MockMvcResultMatchers
                        .status()
                        .is(HttpStatus.NOT_FOUND.value())
                );

        var testPoll = new ru.csu.springweb.entity.Poll(1L,
                "TestPoll",
                LocalDate.now(),
                LocalDate.now().plus(1, ChronoUnit.DAYS),
                true,
                List.of());

        pollRepository.save(testPoll);

        mockMvc
                .perform(MockMvcRequestBuilders.get("/polls/1"))
                .andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(MockMvcResultMatchers.jsonPath("$.pollName")
                        .value(testPoll.getPollName()))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").value(testPoll.getId()));
    }
}
