FROM gradle:7.6.1-jdk11-alpine as build

WORKDIR /build
COPY . /build

RUN gradle assemble --no-daemon

FROM adoptopenjdk/openjdk11:jre-11.0.10_9-alpine

WORKDIR /app
COPY --from=build /build/build/libs/*.jar application.jar

EXPOSE 8080

ENTRYPOINT ["sh", "-c", "java ${JAVA_OPTS} -jar application.jar"]